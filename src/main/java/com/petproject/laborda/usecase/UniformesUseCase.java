package com.petproject.laborda.usecase;


import com.petproject.laborda.entity.Uniforme;
import com.petproject.laborda.repository.UniformeRepository;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class UniformesUseCase {

    private UniformeRepository uniformeRepository;

    public Mono<String> guardarUniforme(Uniforme uniforme) {

        return uniformeRepository.guardarUniforme(uniforme).then(Mono.just("OK"));
    }
}
