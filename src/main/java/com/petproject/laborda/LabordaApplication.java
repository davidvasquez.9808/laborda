package com.petproject.laborda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LabordaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LabordaApplication.class, args);
	}

}
