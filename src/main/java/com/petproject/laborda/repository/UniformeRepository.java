package com.petproject.laborda.repository;


import com.petproject.laborda.entity.Uniforme;
import reactor.core.publisher.Mono;

public interface UniformeRepository {

    Mono<Uniforme> guardarUniforme(Uniforme uniforme);
}
