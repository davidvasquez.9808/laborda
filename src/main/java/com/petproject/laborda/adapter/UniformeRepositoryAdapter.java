package com.petproject.laborda.adapter;

import com.petproject.laborda.dataRepository.UniformeDataRepository;
import com.petproject.laborda.entity.Uniforme;
import com.petproject.laborda.repository.UniformeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;

/**
 * Creado por @author: david.vasquez
 * Fecha: 18/07/2021
 **/
public class UniformeRepositoryAdapter implements UniformeRepository {

    @Autowired
    private UniformeDataRepository uniformeDataRepository;

    @Autowired
    UniformeConverter uniformeConverter;
    @Override
    public Mono<Uniforme> guardarUniforme(Uniforme uniforme) {
        return Mono.just(uniforme)
                .map(uniformeConverter::toData)
                .flatMap(uniformeDataRepository::save)
                .map(uniformeConverter::toEntity);
    }
}
