package com.petproject.laborda.adapter;

import com.petproject.laborda.dataRepository.UniformeData;
import com.petproject.laborda.entity.Uniforme;
import org.reactivecommons.utils.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Creado por @author: david.vasquez
 * Fecha: 18/07/2021
 **/
public class UniformeConverter {

    @Autowired
    private ObjectMapper mapper;
    public UniformeData toData(Uniforme uniforme) {
        return mapper.mapBuilder(uniforme,UniformeData.UniformeDataBuilder.class)
                .build();
    }

    public Uniforme toEntity (UniformeData uniformeData) {
        return mapper.mapBuilder(uniformeData,Uniforme.UniformeBuilder.class)
                .build();
    }
}
