package com.petproject.laborda.controller;

import com.petproject.laborda.entity.Uniforme;
import com.petproject.laborda.usecase.UniformesUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("api_laborda")
public class UniformesController {
    @Autowired
    private UniformesUseCase uniformesUseCase;

    @PostMapping("/uniformes/guardarUniforme")
    public Mono<String> guardarUniforme(@RequestBody Uniforme uniforme){
       return uniformesUseCase.guardarUniforme(uniforme);

    }


}
