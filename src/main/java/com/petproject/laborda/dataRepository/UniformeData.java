package com.petproject.laborda.dataRepository;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Creado por @author: david.vasquez
 * Fecha: 18/07/2021
 **/
@Getter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Document(collection = "Uniforme")
public class UniformeData {
    @Id
    private String id;
    private String talla;
    private String prenda;
    private String color;
}
