package com.petproject.laborda.dataRepository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * Creado por @author: david.vasquez
 * Fecha: 18/07/2021
 **/
public interface UniformeDataRepository extends ReactiveCrudRepository<UniformeData,String> {


}
