package com.petproject.laborda.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;


@Getter
@AllArgsConstructor
@Builder(toBuilder = true)
public class Uniforme {
    private String id;
    private String talla;
    private String prenda;
    private String color;
}
